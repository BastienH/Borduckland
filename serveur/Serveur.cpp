#include "Serveur.hpp"

using namespace sf;

int main(){
	bool done = false;
	short int port = 2000;

	TcpListener listener;
	SocketSelector selector;

	if (listener.listen(port) == Socket::Done){
		std::cout << "Serveur correctement lance." << std::endl;
	}else{
		exit(0);
	}

	selector.add(listener);

	std::vector<TcpSocket*> Clients;

	while(!done){
		if (selector.wait()){
			if (selector.isReady(listener)){

				TcpSocket *socket = new TcpSocket();
				listener.accept(*socket);
				selector.add(*socket);
				Clients.push_back(socket);

				Packet receivePacket;

				if (socket->receive(receivePacket) == Socket::Done){
					std::string pseudo;
					receivePacket >> pseudo;
					std::cout << pseudo << " vient de se connecter. " << std::endl;
				}
			} else {
				 for (unsigned int i = 0; i < Clients.size(); ++i){
					if (selector.isReady(*Clients[i])){
						Packet receivePacket;
						if (Clients[i]->receive(receivePacket) == Socket::Done){
							std::string pseudo;

							receivePacket >> pseudo ;

							std::cout << "Recu du client " << pseudo << std::endl;

							Packet sendPacket;
							sendPacket << pseudo;

							for (unsigned int j = 0; j < Clients.size(); ++j){
								if (i != j){
									Clients[j]->send(sendPacket);
								}
							}

							//std::cout << "Il y a actuellement : " << Clients.size() << " clients de connectes" << std::endl;
						}
					}
				}
			}
		}
	}
	return 0;
}