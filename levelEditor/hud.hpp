#ifndef HUD_HPP
#define HUD_HPP

#include "data.hpp"

class Collidable
{
  private:

    sf::Sprite _sprite;
    int _type;

  public:

    Collidable(const sf::Texture& texture, const int& i, const int& j, const int& type)
    :_type(type)
    {
      _sprite.setTexture(texture);
      _sprite.setPosition(sf::Vector2f(j * taille_obs, i * taille_obs + hMenu));
    }

    void draw(sf::RenderWindow& _window)
    {
      _window.draw(_sprite);
    }

    const int& getType() const
    {
      return _type;
    }

};


class Hud
{
  private:

    sf::RenderWindow& _window;

    //fond

    sf::Texture _textureFond;

    sf::Sprite _spriteFond;

    //interface

    sf::Texture _textureInterface;

    sf::Sprite _spriteInterface;

    sf::Texture _textureMenu;

    sf::Sprite _spriteMenu;

    //blocs

    sf::Texture _textureFixe;

    sf::Texture _textureDestructible;

    sf::Texture _textureMob;

    sf::Texture _textureDelete;

    sf::Texture _textureItem;

    //visualisation

    sf::Sprite _spriteVisual;

    int _choix;

    std::array<std::array<Collidable *, mat_j>, mat_i> _collisionMap;

    bool _fichier, _edition;

  public:

    Hud(sf::RenderWindow&);

    void clickReleased(const sf::Event&);
    void mouseMoved(const sf::Event&);

    void drawButton();

    void addBatiment(Collidable * bat, int i, int j);

    void getEvents();

    void tick();

    bool empty(const int& i, const int& j);

    void generateJSON();

};

enum Objets {FIXE, DESTRUCTIBLE, MOB, DELETE, ITEM};

#endif
