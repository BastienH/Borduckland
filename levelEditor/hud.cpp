#include "hud.hpp"

Hud::Hud(sf::RenderWindow& window)
: _window(window), _choix(-1), _fichier(false), _edition(false)
{
  //fond

  if (!_textureFond.loadFromFile("./assets/fond.png")){}

  _spriteFond.setTexture(_textureFond,true);
  _spriteFond.setTextureRect(sf::IntRect(0,0,wWindow,hWindow));
  _spriteFond.setPosition(sf::Vector2f(0, hMenu));

  if (!_textureMenu.loadFromFile("./assets/menu.png")){}

  _spriteMenu.setTexture(_textureMenu,true);
  _spriteMenu.setTextureRect(sf::IntRect(0,0,wWindow,hMenu));

  //interface

  if (!_textureInterface.loadFromFile("./assets/interface.png")){}

  _spriteInterface.setTexture(_textureInterface,true);
  _spriteInterface.setTextureRect(sf::IntRect(0,0,wWindow,hInterface));
  _spriteInterface.setPosition(sf::Vector2f(0, hWindow + hMenu));

  //blocs

  if (!_textureMob.loadFromFile("./assets/duckmob.png")){}
  if (!_textureFixe.loadFromFile("./assets/rock2.png")){}
  if (!_textureDestructible.loadFromFile("./assets/destroyable.png")){}
  if (!_textureDelete.loadFromFile("./assets/DELETE.png")){}
  if (!_textureItem.loadFromFile("./assets/item.png")){}

  //init de la map

  for(int i = 0; i < mat_i; ++i)
  {
    for(int j = 0; j < mat_j; ++j)
    {
      _collisionMap[i][j] = nullptr;
    }
  }



}

void Hud::clickReleased(const sf::Event& event)
{
  int xClick = event.mouseButton.x;
  int yClick = event.mouseButton.y;
  int ecart = 50, ecart_gauche = 40, ecart_haut = 20, tailleBouton = 100;
  bool emp;
  int i, j;

  Collidable * col;

  //std::cout << xClick << " " << yClick << std::endl;

  if(xClick > 0 && xClick < wWindow && yClick > hWindow && yClick < hWindow + hInterface)
  {

    if(xClick > ecart_gauche && xClick < ecart_gauche + 5 * ecart + 6 * tailleBouton && yClick > hWindow + hMenu + ecart_haut && yClick < hWindow + hMenu + ecart_haut + tailleBouton)
    {


      if(xClick > ecart_gauche && xClick < ecart_gauche + tailleBouton)
      {
        _choix = FIXE;
        _spriteVisual.setTexture(_textureFixe);
      }
      else if(xClick > ecart_gauche + ecart + tailleBouton && xClick < ecart_gauche + 2 * tailleBouton + ecart)
      {
        _choix = DESTRUCTIBLE;
        _spriteVisual.setTexture(_textureDestructible);
      }
      else if(xClick > ecart_gauche + 2 * ecart + 2 * tailleBouton && xClick < ecart_gauche + 3 * tailleBouton + 2 * ecart)
      {
        _choix = MOB;
        _spriteVisual.setTexture(_textureMob);
      }
      else if(xClick > ecart_gauche + 3 * ecart + 3 * tailleBouton && xClick < ecart_gauche + 4 * tailleBouton + 3 * ecart)
      {
        _choix = DELETE;
        _spriteVisual.setTexture(_textureDelete);
      }
      else if(xClick > ecart_gauche + 4 * ecart + 4 * tailleBouton && xClick < ecart_gauche + 5 * tailleBouton + 4 * ecart)
      {
        _choix = ITEM;
        _spriteVisual.setTexture(_textureItem);
      }
      else if(xClick > ecart_gauche + 5 * ecart + 5 * tailleBouton && xClick < ecart_gauche + 6 * tailleBouton + 5 * ecart)
      {
        generateJSON();
      }

      _spriteVisual.setPosition(sf::Vector2f(xClick, yClick + hMenu));

      //std::cout << _choix << std::endl;
    }
  }


  if(_choix != -1)
  {
    if(xClick > taille_obs && xClick < wWindow - taille_obs && yClick > taille_obs + hMenu && yClick < hWindow + hMenu - taille_obs)
    {
      j = xClick / taille_obs;
      i = yClick / taille_obs;

      emp = empty(i, j);

      switch (_choix)
      {
        case FIXE:
          col = new Collidable(_textureFixe, i, j, FIXE);
          break;
        case DESTRUCTIBLE:
          col = new Collidable(_textureDestructible, i, j, DESTRUCTIBLE);
          break;
        case MOB:
          col = new Collidable(_textureMob, i, j, MOB);
          break;
        case DELETE:
          addBatiment(nullptr, i, j);
          break;
        case ITEM:
          col = new Collidable(_textureItem, i, j, ITEM);
          break;
      }

      if(_choix != DELETE && emp)
      {
        addBatiment(col, i, j);
      }

    }
  }
}

void Hud::mouseMoved(const sf::Event& event)
{
  int ecart = 10;
  int taille = 50;

  if(_choix != -1)
  {
    //std::cout << event.mouseMove.x << " " << event.mouseMove.y << std::endl;

    _spriteVisual.setPosition(sf::Vector2f(event.mouseMove.x/taille_obs * taille_obs , event.mouseMove.y/taille_obs * taille_obs + hMenu));
  }

  if(event.mouseMove.y > 0 && event.mouseMove.y < hMenu)
  {
    if(event.mouseMove.x > ecart && event.mouseMove.x < ecart + taille)
    {
      if(!_fichier)
      {
        _fichier = true;
        std::cout << "fichier" << std::endl;
      }
    }
    else if(event.mouseMove.x > 2 * ecart + taille && event.mouseMove.x < 2 * ecart + 2 * taille)
    {
      if(!_edition)
      {
        _edition = true;
        std::cout << "edition" << std::endl;
      }
    }
    else
    {
      _edition = false;
      _fichier = false;
    }

  }

}

void Hud::getEvents()
{
  sf::Event event;

  while (_window.pollEvent(event))
  {
    // fermeture de la fenêtre lorsque l'utilisateur le souhaite
    switch (event.type)
    {
        // fenêtre fermée
        case sf::Event::Closed:
            _window.close();
            break;

        case sf::Event::MouseMoved:

          mouseMoved(event);

          break;

        case sf::Event::MouseButtonReleased:

          clickReleased(event);

          break;

        // on ne traite pas les autres types d'évènements
        default:
            break;
    }
  }

}

void Hud::drawButton()
{
  _window.draw(_spriteMenu);

  _window.draw(_spriteFond);

  for(int i = 1; i < mat_i - 1; ++i)
  {
    for(int j = 1; j < mat_j - 1; ++j)
    {
      if(_collisionMap[i][j])
      {
        _collisionMap[i][j]->draw(_window);
      }
    }
  }

  if(_choix != -1)
  {
    _window.draw(_spriteVisual);
  }

  _window.draw(_spriteInterface);
}

void Hud::addBatiment(Collidable * bat, int i, int j)
{
  _collisionMap[i][j] = bat;
}

void Hud::tick()
{
  getEvents();
  drawButton();
}

bool Hud::empty(const int& i, const int& j)
{
  return (_collisionMap[i][j] == nullptr);
}

void Hud::generateJSON()
{
  bool first = true;
  std::ofstream fic;

  std::ifstream fichier("salle.json");

  std::ifstream ifs("salle.json");
  Json::Reader reader;
  Json::Value obj;
  reader.parse(ifs, obj); // reader can also read strings
  int nb = 0;

  //si fichier existe
  if(fichier)
  {
    nb = obj["nbRooms"].asUInt();
    //ouverture du fichier
    fic.open("salle.json", std::ios::in | std::ios::out | std::ios::ate);
    fic.seekp(0, std::ios::beg);
  }
  else
  {
    fic.open("salle.json");
  }


  ++nb;

  fic << "{\n  \"nbRooms\":" << nb;

  if(fichier)
  {
    fic.seekp(-3, std::ios::end);
    fichier.close();
  }



  fic <<",\n  \"" << nb - 1 << "\":{" << std::endl;

  fic << "  \"items\":\n    [" << std::endl;

  //ITEM
  for(int i = 1; i < mat_i - 1; ++i)
  {
    for(int j = 1; j < mat_j - 1; ++j)
    {
      if(_collisionMap[i][j] && _collisionMap[i][j]->getType() == ITEM)
      {
        if(first)
        {
          first = false;
          fic << "\n      { \"name\" : \"item\", \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"atk\" : 5, \"atkSpeed\" : 10, \"speed\" : 1, \"hp\" : 4 }\n";
        }
        else
        {
          fic << ",\n      { \"name\" : \"item\", \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"atk\" : 5, \"atkSpeed\" : 10, \"speed\" : 1, \"hp\" : 4 }";
        }
      }
    }
  }


  first = true;

  fic << "\n      ],\n  \"mob\":\n    [" << std::endl;

  //MOB
  for(int i = 1; i < mat_i - 1; ++i)
  {
    for(int j = 1; j < mat_j - 1; ++j)
    {
      if(_collisionMap[i][j] && _collisionMap[i][j]->getType() == MOB)
      {
        if(first)
        {
          first = false;
          fic << "      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << "}";
        }
        else
        {
          fic << ",\n      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << "}";
        }
      }
    }
  }

  fic << "\n    ],\n  \"obs\":\n    [" << std::endl;

  first = true;

  //FIXE
  for(int i = 1; i < mat_i - 1; ++i)
  {
    for(int j = 1; j < mat_j - 1; ++j)
    {
      if(_collisionMap[i][j] && _collisionMap[i][j]->getType() == FIXE)
      {
        if(first)
        {
          fic << "      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"destroyable\" : false }";
          first = false;
        }
        else
        {
          fic << ",\n      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"destroyable\" : false }";
        }

      }
    }
  }

  //DESTRUCTIBLE
  for(int i = 1; i < mat_i - 1; ++i)
  {
    for(int j = 1; j < mat_j - 1; ++j)
    {
      if(_collisionMap[i][j] && _collisionMap[i][j]->getType() == DESTRUCTIBLE)
      {
        if(first)
        {
          fic << "      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"destroyable\" : true }";
          first = false;
        }
        else
        {
          fic << ",\n      { \"x\" : " << j * taille_obs << ", \"y\" : " << i * taille_obs << ", \"destroyable\" : true }";
        }
      }
    }
  }



  fic <<"\n    ]\n  }\n}" << std::endl;


  fic.close();

  std::cout << "Generation terminee" << std::endl;
}
