#include "data.hpp"
#include "hud.hpp"


int main(int, char**)
{

  //création de la fenêtre
  sf::RenderWindow window(sf::VideoMode(wWindow, hWindow + hInterface + hMenu), "Level Editor");

  //60 fps
  window.setFramerateLimit(60);

  Hud hud(window);

  // on fait tourner le programme tant que la fenêtre n'a pas été fermée
  while (window.isOpen())
  {
    // effacement de la fenêtre en noir
    window.clear(sf::Color::Black);

    hud.tick();

    // fin de la frame courante, affichage de tout ce qu'on a dessiné
    window.display();
  }

  return 0;
}
