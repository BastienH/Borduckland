#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "data.hpp"

class Interface{

  private:

    std::vector<std::vector<int>> map;
    sf::RenderWindow& window;

    sf::Texture textureLeft;
    sf::Texture textureRight;
    sf::Texture textureLeftCanister;
    sf::Texture textureRightCanister;

  public:

    Interface(sf::RenderWindow& _window);

    void drawMap() const;

    void majMap(int i, int j, int type);

    void drawLives(int hp, int hpmax) const;

};


#endif
