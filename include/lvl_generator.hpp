#ifndef LVL_GENERATOR_HPP
#define LVL_GENERATOR_HPP

#include "room.hpp"

class RoomLoad;

class LevelGeneration{

  private:

    std::array<std::array<Room *, j_max>, i_max> rooms;
    std::vector<std::vector<int>> takenPositions;
    int _nbRoom;

    sf::RenderWindow& _window;

    std::vector<RoomLoad> _loaded;

  public:

    LevelGeneration(const int& nbRoom, sf::RenderWindow& window);
    ~LevelGeneration();

    void start(int seed);
    void createRooms();
    std::vector<int> newPosition();
    std::vector<int> newSelectivePosition();
    int numberOfNeighbors(const std::vector<int>& checkingPos);
    void setDoors();
    void draw(int i, int j);

    Room * & getRoom(int i, int j);

    void moveView(std::vector<RoomLoad>::iterator& loadedRoom);

    void spawnPlayer(int i, int j);

    void tick();

};

class RoomLoad
{
  private:
    int _i;
    int _j;
  public:
    RoomLoad(int i, int j):_i(i), _j(j){}
    friend LevelGeneration;
};

#endif
