#ifndef MOB_HPP
#define MOB_HPP

#include "pathfinding.hpp"
#include "player.hpp"


class Room;
class Player;
class Tuile;

class Mob : public Movable, public Actor
{
  public:
    Mob(sf::RenderWindow& window, int x, int y, int w, int h);

    void init();
    void tick(Room& r);
    void damaged(const int& dmg);

    Player * getNearestPlayer(std::list<Player *>& listePerso);
    std::vector<std::vector<int>> initColMap(Player * cible, std::array<std::array<Collidable *, mat_j>, mat_i>& matriceCollisions);
    void setMobSpeed(const std::vector<Tuile>& Chemin);
    bool isCollidingWithPlayer(Player * cible);
};

#endif
