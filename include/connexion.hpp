#ifndef CONNEXION_HPP
#define CONNEXION_HPP

#include "data.hpp"

class Connexion{
    private:
	    std::string _ip;// IP du serveur
	    short int _port;// Port du serveur
	    std::string _pseudo; // pseudo du joueur
	    sf::TcpSocket _socket; // socket utilisée
	    bool _isConnected = false; // booleen de connexion au serveur

    public:
        Connexion(std::string ip = "127.0.0.1",int port = 2000, std::string pseudo = "Pixel");
        bool lancer();
        void envoyer();
        void recevoir();
};

#endif
