#ifndef ROOM_HPP
#define ROOM_HPP

#include "obstacles.hpp"
#include "player.hpp"
#include "scene.hpp"

class Player;

class Room : public Scene
{
  private:

    std::array<std::array<Collidable *, mat_j>, mat_i> _collisionMap;

    sf::RenderWindow& _window;

    bool _itemPickable;
    bool _ended;

    std::list<Collidable *> _doors;

    std::list<Player *> _listPlayer;

    sf::Texture _textureRoom;
    sf::Sprite _spriteRoom;

    bool _isOrigin;


  public:

    Room(sf::RenderWindow& window, const bool& origin = false);
    ~Room();

    void init();
    void tick();

    std::array<std::array<Collidable *, mat_j>, mat_i>& getColMap();

    const bool& isItemPickable() const;
    void setItemPickable(const bool& itemPickable);

    void addObstacle(const std::string& fic, const int& x, const int& y, const int& w, const int& h, const bool& destroyable);

    void addDoor(const std::string& fic, const int& x, const int& y, const int& w, const int& h);

    Collidable *& getColObj(int i, int j);

    std::list<Player *>& getListPlayer();

    void build_room(const std::string& file);

    void affCol()
    {
      for(int i = 0; i < mat_i; ++i)
      {
        for(int j = 0; j < mat_j; ++j)
        {

          std::cout << (_collisionMap[i][j] != nullptr) << " ";
        }
        std::cout << std::endl;
      }
    }

    sf::Sprite& getSprite();

    void draw();

    void checkObs();

    void addActor(Actor * a);

    void delActor(Actor * a);

    void leaveColMat(Collidable * ptr);

    void joinColMat(Collidable * ptr);

    void newPlayer(Player * p);

    void delPlayer(Player * p);

    std::list<Player *>::iterator& delPlayerIt(std::list<Player *>::iterator& courPlayer);

    void openDoors();

    void setOrigin(const bool& value)
    {
      _isOrigin = value;
    }

};

#endif
