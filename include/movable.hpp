#ifndef MOVABLE_HPP
#define MOVABLE_HPP

#include "collidable.hpp"


class Movable : public Collidable
{
  protected:

    int _speed;
    int _vx;
    int _vy;
    int _damages;

  public:

    Movable(sf::RenderWindow& window, int x, int y, int w, int h, int hpMax, int hp);

    const int& getSpeed() const;
    void setSpeed(const int& speed);

    const int& getVx() const;
    const int& getVy() const;

    void setVx(const int vx);
    void setVy(const int vy);

    const int& getDamages() const;
    void setDamages(const int& damages);

      void move();

      void doMovement(const std::array<std::array<Collidable *, mat_j>, mat_i>& colMap);

      void applySpeed();

      void cancel();

      void divideSpeed(Collidable * obj);

      Collidable * colliding(const std::array<std::array<Collidable *, mat_j>, mat_i>& matrice_collisions) const;

      void tp(int vx, int vy);
};

#endif
