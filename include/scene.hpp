#ifndef SCENE_HPP
#define SCENE_HPP

#include "actor.hpp"

class Actor;

class Scene
{
protected:

  std::list<Actor *> _entities;

public:
  Scene(){}
  virtual ~Scene(){};

  virtual void init() = 0;
  virtual void tick() = 0;

};

#endif
