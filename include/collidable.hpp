#ifndef Collidable_HPP
#define Collidable_HPP

#include "data.hpp"
#include "sound.hpp"

class Collidable
{
  protected:
    int _x;
    int _y;
    int _w;
    int _h;

    int _hpMax;
    int _hp;

    sf::RenderWindow& _window;
    sf::Sprite _sprite;
    sf::Texture _texture;

  public:

  Collidable(sf::RenderWindow& window, int x, int y, int w, int h, int hpMax, int hp);
  virtual ~Collidable(){}

  void setX(const int& x);
  const int& getX() const;

  void setY(const int& y);
  const int& getY() const;

  const int& getW() const;
  const int& getH() const;

  const int& getHpMax() const;
  void setHpMax(const int& hpmax);

  const int& getHp() const;

  void majHp(const int& up);

  sf::Sprite& getSprite();

  virtual void damaged(const int&){};
};

#endif
