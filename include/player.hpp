#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "projectile.hpp"
#include "interface.hpp"

class Projectile;
class Room;

class Player : public Movable, public Actor
{
  private:

    //bool de deplacement
    bool _up;
    bool _down;
    bool _left;
    bool _right;

    //bool des projectiles
    bool _projUp;
    bool _projDown;
    bool _projLeft;
    bool _projRight;

    int _speedProj;

    int _cdTir;
    int _cdTirMax;

    int _frameInv;

    sf::Texture _textureTir;

    Interface _ihm;

  public:

    sf::View viewPlayer1;

    Player(sf::RenderWindow& window, int x, int y, int w, int h);

    void setCdTirMax(const int& cd);
    const int& getCdTirMax() const;

    void init();
    void tick(Room& r);

    void getInputs();
    void eventKeyPressed(const sf::Event& event);
    void eventKeyReleased(const sf::Event& event);

    //speed
    void setSpeedInput();

    Projectile * newProj();

    sf::Texture& getTextureTir();

    void damaged(const int& dmg);

    void drawIhm() const;

    void enterRoomMap(int i, int j);
    void leaveRoomMap(int i, int j);


};


#endif
