#ifndef PATHFINDING_HPP
#define PATHFINDING_HPP

#define FLT_MAX 9999 // infini

#include <stack>
#include <vector>
#include <array>
#include <iostream>
#include <cmath>

#include "room.hpp"
#include "collidable.hpp"
#include "tuile.hpp"

class Tuile;

class PathFinding {
public:

	static bool isValid(int x, int y, std::vector<std::vector<int>> matriceCollisions);

	static bool isDestination(int x, int y, Tuile dest);

	static double calculateH(int x, int y, Tuile dest);

	static std::vector<Tuile> makePath(std::array<std::array<Tuile, mat_i>, mat_j> map, Tuile dest);

	static std::vector<Tuile> aStar(Tuile player, Tuile dest, std::vector<std::vector<int>> matriceCollisions);
};

#endif
