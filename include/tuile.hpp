#ifndef TUILE_HPP
#define TUILE_HPP

#include <string>

#include "pathfinding.hpp"

class PathFinding;

class Tuile {

    friend  class PathFinding;

    private:

        int _x;
        int _y;
        int _parentX;
        int _parentY;
        float _gCost;
        float _hCost; 
        float _fCost;

    public:

        Tuile();
        Tuile(int x, int y);
        float getFCost() const;
        int getX() const;
        int getY() const;
        void setX(int x);
        void setY(int y);

};

bool operator<(const Tuile& lhs, const Tuile& rhs);

#endif