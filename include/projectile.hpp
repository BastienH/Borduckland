#ifndef PROJECTILE_HPP
#define PROJECTILE_HPP


#include "actor.hpp"
#include "movable.hpp"

#include "room.hpp"

class Room;

class Projectile : public Movable, public Actor
{
  private:

    Collidable * _player;

  public:
    Projectile(sf::RenderWindow& window, Collidable * player);

    void init();
    void tick(Room& r);

    void damaged(const int&){};
};

#endif
