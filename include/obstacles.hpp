#ifndef OBSTACLES_HPP
#define OBSTACLES_HPP

#include "collidable.hpp"

class Obstacles : public Collidable
{

  private:

    bool _canBeDamaged;

  public:

    //methodes

    //constructeur
    Obstacles(const std::string& fic, sf::RenderWindow& window, const int& x, const int& y, const int& w, const int& h, const bool& destroyable);

    void damaged(const int&);
};

#endif
