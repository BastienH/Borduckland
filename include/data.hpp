#ifndef DATA_HPP
#define DATA_HPP

#include <string>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include <iterator>
#include <vector>
#include <SFML/System.hpp>
#include <cmath>
#include <fstream>
#include <jsoncpp/json/json.h>
#include <memory>
#include <array>
#include <type_traits>
#include <SFML/Network.hpp>

//taille de la fenetre
const int wWindow = 1000;
const int hWindow = 500;

const int hInterface = 150;

//taille de la matrices des salles
const int i_max = 11;
const int j_max = 11;

//taille de la matrice de col
const int taille_grille = 50;
const int taille_obs = 50;
const int mat_i = hWindow / taille_grille;
const int mat_j = wWindow / taille_grille;

//cd tirs
const int cd_max = 30;
const int frame_inv_max = 30;

const int marge_hitbox = 4;

const int origine_x = i_max/2;
const int origine_y = j_max/2;



#endif
