#ifndef SOUND_HPP
#define SOUND_HPP

#include <SFML/Audio.hpp>

class Bruitage{

  private:

    sf::SoundBuffer bufferDegats;

  public:

    Bruitage();

    sf::Sound degats;

};

#endif
