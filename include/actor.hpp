#ifndef ACTOR_HPP
#define ACTOR_HPP

class Room;

class Actor
{
  public:
    Actor(){}
    virtual ~Actor(){};

    virtual void init() = 0;
    virtual void tick(Room& r) = 0;
};

#endif
