#ifndef ITEM_HPP
#define ITEM_HPP

#include "player.hpp"


class Item : public Actor, public Collidable
{
  private:

    int _dmg;
    int _atkSpeed;
    int _speed;
    int _hp;

  public:

    Item(const std::string& fic, sf::RenderWindow& window, const int& x, const int y, const int& w, const int& h, const int& dmg, const int& atkSpeed, const int& speed, const int& hp);
    void pickUp(Player * cible);
    bool isPlayerNear(Player * cible);
    void init();
    void tick(Room& r);

};


#endif
