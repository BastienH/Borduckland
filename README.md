# Borduckland

## Demande de sujet de projet

### Nom des étudiants :
    AMORIM Dylan
    HERREROS Bastien

### Prérequis : 
* [SFML](https://www.sfml-dev.org/download-fr.php)

* [JsonCpp](https://en.wikibooks.org/wiki/JsonCpp)

## Présentation du projet :

Nous souhaitons développer un jeu multijoueur 2D en C++ dans lequel les niveaux seront générés de façon procédurale et dans lequel les joueurs devront s’affronter dans le but d’être le dernier survivant.

* Chaque niveau contiendra des salles et chaque salle possédera des ennemis et des objets. Les salles seront reliées entre elles mais pas toutes en fonction de la disposition de ces dernières (cf. la génération de la carte).

* Chaque joueur aura une barre de vie, une interface affichant les objets qu’il possède et ses propres statistiques.

* Chaque ennemi aura une barre de vie, ses propres statistiques et une “intelligence artificielle”.

Pour ce projet nous nous inspirons majoritairement d’un autre jeu appelé : The binding of Isaac.

## Aspect graphique du jeu Binding of Isaac :
<div style="text-align:center">
    <img src="https://img.igen.fr/2017/1/macgpic-1484139891-101962335648183-sc-jpt.jpg" alt="photo jeu binding of isaac" />
</div>

## Références utilisées :

### Génération procédurale du donjon :
* [Pseudo-code](https://bindingofisaacrebirth.gamepedia.com/Level_Generation)

* [Video Unity](https://www.youtube.com/watch?v=nADIYwgKHv4)

* [Tutoriel sur le pathfinding A*](https://www.raywenderlich.com/3016-introduction-to-a-pathfinding)

* [Exemple d'implémentation que nous avons choisie](https://github.coventry.ac.uk/jansonsa/D1/blob/master/pokemonGUI/pokemonGUI/Cordinate.h)

