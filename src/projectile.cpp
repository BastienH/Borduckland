#include "projectile.hpp"

Projectile::Projectile(sf::RenderWindow& window, Collidable * player)
: Movable(window, 0, 0, 15, 15, 10, 10), _player(player)
{
  _sprite.setTexture(((Player *)player)->getTextureTir());
  _sprite.setScale(sf::Vector2f(15.0 / ((Player *)player)->getTextureTir().getSize().x, 15.0 / ((Player *)player)->getTextureTir().getSize().y));
}

void Projectile::init()
{

}

void Projectile::tick(Room& r)
{
  move();

  if(_x < 0 || _x > wWindow || _y < 0 || _y > hWindow)
  {
    r.delActor(this);
  }
  else
  {  
    _window.draw(_sprite);

    Collidable * obj = colliding(r.getColMap());

    //std::cout << (obj != nullptr) << std::endl;

    if(obj && obj != _player)
    {
      obj->damaged(_damages);
      r.delActor(this);
    }
  }
}
