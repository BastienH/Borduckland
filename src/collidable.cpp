#include "collidable.hpp"

Collidable::Collidable(sf::RenderWindow& window, int x, int y, int w, int h, int hpMax, int hp)
: _x(x), _y(y), _w(w), _h(h), _hpMax(hpMax), _hp(hp), _window(window)
{

}

void Collidable::setX(const int& x)
{
  _x = x ;
}

void Collidable::setY(const int& y)
{
  _y = y ;
}

const int& Collidable::getX() const
{
  return _x;
}

const int& Collidable::getY() const
{
  return _y;
}

const int& Collidable::getW() const
{
  return _w;
}

const int& Collidable::getH() const
{
  return _h;
}

const int& Collidable::getHpMax() const
{
  return _hpMax;
}
void Collidable::setHpMax(const int& hpmax)
{
  _hpMax = hpmax;
}

const int& Collidable::getHp() const
{
  return _hp;
}

void Collidable::majHp(const int& up)
{

  if(_hpMax + up <= 24)
  {
    _hpMax += up;
  }
  else
  {
    _hpMax = 24;
  }

  if(up > 0)
  {
    _hp += up;
  }


  if(_hp > _hpMax)
  {
    _hp = _hpMax;
  }
}

sf::Sprite& Collidable::getSprite()
{
  return _sprite;
}
