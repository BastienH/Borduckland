#include "room.hpp"

#include "ennemie.hpp"
#include "item.hpp"


Room::Room(sf::RenderWindow& window, const bool& origin):
Scene(), _window(window), _itemPickable(false), _ended(false), _isOrigin(origin)
{
  //chargement texture fond
  _textureRoom.loadFromFile("assets/room.png");
  _spriteRoom.setTexture(_textureRoom);
  _spriteRoom.setScale(sf::Vector2f((float)wWindow / _textureRoom.getSize().x, (float)hWindow / _textureRoom.getSize().y));

  //init matrice de collision
  for(int i = 0; i < mat_i; ++i)
  {
    for(int j = 0; j < mat_j; ++j)
    {
      _collisionMap[i][j] = nullptr;
    }
  }



  //construction des obstacles
  build_room("salles/Salles.json");

}

Room::~Room()
{
  Collidable * tmp;

  //affCol();

  for(int i = 0; i < mat_i; ++i)
  {
    for(int j = 0; j < mat_j; ++j)
    {
      tmp = _collisionMap[i][j];

      if(tmp)
      {
        leaveColMat(tmp);
        //affCol();
        delete tmp;
      }

    }
  }
}

void Room::init()
{

}

void Room::tick()
{
  checkObs();
  draw();

  //clean list actor
  for(std::list<Actor *>::iterator it = _entities.begin(); it != _entities.end(); ++it)
  {
    if(*it)
    {
      (*it)->tick(*this);
    }

    if(*it == nullptr)
    {
      it = _entities.erase(it);
    }
  }

  if(_entities.size() <= _listPlayer.size() + 1)
  {
    openDoors();
  }
  //affCol();
}


std::array<std::array<Collidable *, mat_j>, mat_i>& Room::getColMap()
{
  return _collisionMap;
}

const bool& Room::isItemPickable() const
{
  return _itemPickable;
}

void Room::setItemPickable(const bool& itemPickable)
{
  _itemPickable = itemPickable;
}


void Room::addObstacle(const std::string& fic, const int& x, const int& y, const int& w, const int& h, const bool& destroyable)
{
  int i = y / taille_grille;
  int j = x / taille_grille;

  Obstacles * obs = new Obstacles(fic, _window, x, y, w, h, destroyable);

  //ajout dans la matrice
  if(h > taille_grille || w > taille_grille)
  {

    if(h <= taille_grille)
    {
      for(j = x / taille_grille; j < (x + w) / taille_grille; ++j)
      {
        _collisionMap[i][j] =  obs;
      }
    }
    else if(w <= taille_grille)
    {
      for(i = y / taille_grille; i < (y + h) / taille_grille; ++i)
      {
        _collisionMap[i][j] =  obs;
      }
    }
    else
    {
      for(i = y / taille_grille; i < (y + h) / taille_grille; ++i)
      {
        for(j = x / taille_grille; j < (x + w) / taille_grille; ++j)
        {
          _collisionMap[i][j] =  obs;
        }
      }
    }
  }
  else
  {
    _collisionMap[i][j] =  obs;
  }

}

void Room::addDoor(const std::string& fic, const int& x, const int& y, const int& w, const int& h)
{
  int i = y / taille_grille;
  int j = x / taille_grille;

  Obstacles * obs = new Obstacles(fic, _window, x, y, w, h, false);


  //ajout dans la matrice
  if(h > taille_grille || w > taille_grille)
  {

    if(h <= taille_grille)
    {
      for(j = x / taille_grille; j < (x + w) / taille_grille; ++j)
      {
        _collisionMap[i][j] =  obs;
      }
    }
    else if(w <= taille_grille)
    {
      for(i = y / taille_grille; i < (y + h) / taille_grille; ++i)
      {
        _collisionMap[i][j] =  obs;
      }
    }
    else
    {
      for(i = y / taille_grille; i < (y + h) / taille_grille; ++i)
      {
        for(j = x / taille_grille; j <= (x + w) / taille_grille; ++j)
        {
          _collisionMap[i][j] =  obs;
        }
      }
    }
  }
  else
  {
    _collisionMap[i][j] =  obs;
  }

  _doors.push_back(obs);

}

Collidable *& Room::getColObj(int i, int j)
{
  return _collisionMap[i][j];
}

std::list<Player *>& Room::getListPlayer()
{
  return _listPlayer;
}

void Room::build_room(const std::string& file)
{
  std::ifstream ifs(file);
  Json::Reader reader;
  Json::Value obj;
  reader.parse(ifs, obj); // reader can also read strings
  std::string nb, name;
  int x, y, atk, atkSpeed, speed, hp;
  bool destroyable;

  Mob * m;

  //skin obs
  int nbAssets = 3;
  std::string asset[] = {"assets/rock.png", "assets/rock1.png", "assets/rock2.png"};

  //construction d'une salle random

  if(_isOrigin)
  {
    nb = "0";
  }
  else
  {
    nb = std::to_string(rand()%(obj["nbRooms"].asUInt() - 1)+1);
  }

  //lecture dans le json

  //item


  const Json::Value& item = obj[nb]["items"];

  if(item.size() > 0)
  {
    x = item[0]["x"].asInt();
    y = item[0]["y"].asInt();

    atk = item[0]["atk"].asInt();
    atkSpeed = item[0]["atkSpeed"].asInt();
    speed = item[0]["speed"].asInt();
    hp = item[0]["hp"].asInt();
    name = item[0]["name"].asString();

    addActor(new Item("assets/items/" + name +".png", _window, x, y, taille_grille,taille_grille, atk, atkSpeed, speed, hp));
  }


  //std::cout << name << std::endl;


  //mob
  const Json::Value& mob = obj[nb]["mob"];

  for (unsigned int iroom = 0; iroom < mob.size(); iroom++)
  {
    x = mob[iroom]["x"].asInt();
    y = mob[iroom]["y"].asInt();

    m = new Mob(_window, x, y, taille_grille,taille_grille);
    m->setSpeed(5);

    joinColMat((Collidable*)m);
    addActor(m);
  }

  //obs
  const Json::Value& obs = obj[nb]["obs"];

  for (unsigned int iroom = 0; iroom < obs.size(); iroom++)
  {
    x = obs[iroom]["x"].asInt();
    y = obs[iroom]["y"].asInt();
    destroyable = obs[iroom]["destroyable"].asBool();

    if(destroyable)
    {
      addObstacle("assets/destroyable.png", x, y, taille_obs, taille_obs, destroyable);
    }
    else
    {
      addObstacle(asset[rand()%nbAssets], x, y, taille_obs, taille_obs, destroyable);
    }

  }

}

void Room::newPlayer(Player * p)
{
  addActor(p);
  _listPlayer.push_back(p);
  joinColMat((Collidable*)p);
}

void Room::delPlayer(Player * p)
{
  delActor(p);

  std::list<Player *>::iterator it = std::find(_listPlayer.begin(), _listPlayer.end(), p);
  _listPlayer.erase(it);

  leaveColMat((Collidable*)p);


}

std::list<Player *>::iterator& Room::delPlayerIt(std::list<Player *>::iterator& courPlayer)
{
  delActor(*courPlayer);
  leaveColMat((Collidable*)(*courPlayer));

  courPlayer = _listPlayer.erase(courPlayer);

  return courPlayer;
}

sf::Sprite& Room::getSprite()
{
  return _spriteRoom;
}

void Room::draw()
{
  _window.draw(getSprite());

  //affCol();

  for(int k = 0; k < mat_i; ++k)
  {
    for(int l = 0; l < mat_j; ++l)
    {
      if(getColObj(k, l))
      {
        _window.draw(getColObj(k, l)->getSprite());
      }
    }
  }
  _window.draw(getColObj(0, 0)->getSprite());
  _window.draw(getColObj(0, mat_j - 1)->getSprite());
  _window.draw(getColObj(mat_i - 1, 0)->getSprite());
  _window.draw(getColObj(mat_i - 1, mat_j - 1)->getSprite());
}

void Room::checkObs()
{
  for(int k = 0; k < mat_i; ++k)
  {
    for(int l = 0; l < mat_j; ++l)
    {
      if(getColObj(k, l) && getColObj(k, l)->getHp() <= 0)
      {
        leaveColMat(getColObj(k, l));
      }
    }
  }
}


void Room::addActor(Actor * a)
{
  _entities.push_back(a);
}

void Room::delActor(Actor * a)
{
  std::list<Actor *>::iterator it = _entities.begin();

  while(it != _entities.end() && *it != a)
  {
    ++it;
  }

  if(it != _entities.end())
  {
    *it = nullptr;
    //_entities.erase(it);
  }
}

void Room::leaveColMat(Collidable * ptr)
{
  for(int iplayer = (ptr->getY() / taille_grille); iplayer <= ((ptr->getY() + ptr->getH()) / taille_grille); ++iplayer)
  {
    for(int jplayer = (ptr->getX() / taille_grille); jplayer <= ((ptr->getX() + ptr->getW()) / taille_grille); ++jplayer)
    {
      if(iplayer >= 0 && iplayer < mat_i && jplayer >=0 && jplayer < mat_j && _collisionMap[iplayer][jplayer] == ptr)
      {
        _collisionMap[iplayer][jplayer] = nullptr;
      }
    }
  }
}

void Room::joinColMat(Collidable * ptr)
{

  //std::cout << ptr->getH() << std::endl;
  for(int iplayer = (ptr->getY() / taille_grille); iplayer <= ((ptr->getY() + ptr->getH()) / taille_grille); ++iplayer)
  {
    for(int jplayer = (ptr->getX() / taille_grille); jplayer <= ((ptr->getX() + ptr->getW()) / taille_grille); ++jplayer)
    {
      if(iplayer >= 0 && iplayer < mat_i && jplayer >=0 && jplayer < mat_j && _collisionMap[iplayer][jplayer] == nullptr)
      {
        _collisionMap[iplayer][jplayer] = ptr;
      }
    }
  }
}


void Room::openDoors()
{
  if(!_ended)
  {
    for(std::list<Collidable *>::iterator it = _doors.begin(); it != _doors.end(); ++it)
    {
      for(int i = (*it)->getY() / taille_grille; i <= ((*it)->getY() + (*it)->getH()) / taille_grille; ++i)
      {
        for(int j = (*it)->getX() / taille_grille; j <= ((*it)->getX() + (*it)->getW()) / taille_grille; ++j)
        {
          if(_collisionMap[i][j] == *it)
          {
            _collisionMap[i][j] =  nullptr;
          }
        }
      }
      //delete *it;
    }
    _ended = true;

    if(!_isOrigin)
      _itemPickable = true;
  }
}
