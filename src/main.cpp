#include "lvl_generator.hpp"
#include "connexion.hpp"

int main(int argc, char** argv){

  //création de la fenêtre
  sf::RenderWindow window(sf::VideoMode(wWindow, hWindow + hInterface), "Borduckland");
  Connexion * co;

  if(argc>=2){
    co = new Connexion("127.0.0.1",2000,argv[2]);
  }else{
    co = new Connexion();
  }
    

  co->lancer();

  //60 fps
  window.setFramerateLimit(60);

  //génération d'un niveau de 20 salles
  LevelGeneration lvl(20, window);

  if(argc > 1)
  {
    lvl.start(atoi(argv[1]));
  }
  else
  {
    lvl.start(1234);
  }

  //std::cout << i_max/2 << " " << j_max/2 << std::endl;

  lvl.spawnPlayer(origine_x, origine_y);

  // on fait tourner le programme tant que la fenêtre n'a pas été fermée
  while (window.isOpen())
  {

    // effacement de la fenêtre en noir
    window.clear(sf::Color::Black);

    lvl.tick();

    // fin de la frame courante, affichage de tout ce qu'on a dessiné
    window.display();

    //Envoie et reception de donnees du serveur
    co->envoyer();
    co->recevoir();
  }

  return 0;
}
