#include "connexion.hpp"
        
Connexion::Connexion(std::string ip,int port, std::string pseudo):_ip(ip),_port(port),_pseudo(pseudo),_isConnected(false){}

bool Connexion::lancer(){
    if (_socket.connect(_ip,_port) == sf::Socket::Done){
		std::cout << "Connecte au serveur ! " << std::endl;
		_socket.setBlocking(false);
		sf::Packet sendPacket;
		sendPacket << _pseudo;
		_socket.send(sendPacket);
		_isConnected = true;
	}else{
		std::cout << "Serveur DOWN ou inaccessible " << std::endl;
	}
    return _isConnected;
}

void Connexion::envoyer(){
    sf::Packet sendPacket;
    sendPacket << _pseudo ;
    _socket.send(sendPacket);
}

void Connexion::recevoir(){
    sf::Packet receivePacket;
    std::string pseudo;
    if (_socket.receive(receivePacket) == sf::Socket::Done){
			receivePacket >> pseudo;
			std::cout << "Le joueur " << pseudo << " est là.";
	}
}