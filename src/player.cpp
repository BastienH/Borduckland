#include "player.hpp"

#include "projectile.hpp"


Player::Player(sf::RenderWindow& window, int x, int y, int w, int h)
: Movable(window, x + marge_hitbox/2, y + marge_hitbox/2, w - marge_hitbox, h - marge_hitbox, 6, 6) , _up(0), _down(0), _left(0), _right(0), _projUp(0), _projDown(0), _projLeft(0), _projRight(0), _speedProj(5), _cdTir(0), _cdTirMax(cd_max), _frameInv(0), _ihm(window), viewPlayer1(sf::FloatRect(0, 0, wWindow, hWindow))
{
  //chargement de la texture des projectiles
  _textureTir.loadFromFile("assets/larme.png");

  //chargement de la texture du personnage
  _texture.loadFromFile("assets/duckman.png");
  _sprite.setTexture(_texture);

  //redimensionnement
  _sprite.setScale(sf::Vector2f((float)w / _texture.getSize().x, (float)h / _texture.getSize().y));

  //position
  _sprite.setPosition(sf::Vector2f(x, y));
}

void Player::setCdTirMax(const int& cd)
{
  _cdTirMax = cd;
}

const int& Player::getCdTirMax() const
{
  return _cdTirMax;
}

void Player::init()
{

}

void Player::tick(Room& r)
{
  drawIhm();

  getInputs();
  setSpeedInput();

  r.leaveColMat((Collidable*)this);
  doMovement(r.getColMap());
  r.joinColMat((Collidable*)this);

  Projectile * p = newProj();

  if(p)
  {
    r.addActor(p);
    //std::cout << p << std::endl;
  }

  if(_hp <= 0)
  {
    r.delActor(this);
    r.leaveColMat((Collidable *) this);
  }

  if(_frameInv != 0){

  --_frameInv;

}


}


Projectile * Player::newProj()
{
  Projectile * p = nullptr;

  //vitesse du projectile
  int vx = (_projRight - _projLeft) * _speedProj;
  int vy = (_projDown - _projUp) * _speedProj;

  //si vitesse non nulle et que cd = 0
  if(_cdTir == 0)
  {
    if(vx || vy)
    {
      //création d'un proj
      p = new Projectile(_window, (Collidable *)this);

      //set de la vitesse
      p->setVx(vx);
      p->setVy(vy);

      //set de la position du sprite
      p->getSprite().setPosition(sf::Vector2f(_x + _w * 2.0/3, _y + 10));
      p->setX(_x + _w/2);
      p->setY(_y + 10);

      //degats
      p->setDamages(_damages);
      //std::cout << player.getDamages() << " " << p.getDamages() << std::endl;

      //reset du cd
      _cdTir = _cdTirMax;
    }
  }
  else
  {
    --_cdTir;
  }

  return p;

}


void Player::setSpeedInput()
{
  _vx = (_right - _left) * _speed;
  _vy = (_down - _up) * _speed;
}

void Player::getInputs()
{
  sf::Event event;

  while (_window.pollEvent(event))
  {
    // fermeture de la fenêtre lorsque l'utilisateur le souhaite
    switch (event.type)
    {
        // fenêtre fermée
        case sf::Event::Closed:
            _window.close();
            break;

        // touche pressée
        case sf::Event::KeyPressed:

          eventKeyPressed(event);

          break;

        //touche relachée
        case sf::Event::KeyReleased:

          eventKeyReleased(event);

          break;

        // on ne traite pas les autres types d'évènements
        default:
            break;
    }
  }
}


void Player::eventKeyPressed(const sf::Event& event){

  switch (event.key.code)
  {
    case sf::Keyboard::Q:
      if(!_left)
      {
        _left = 1;
      }
      break;

    case sf::Keyboard::D:
      if(!_right)
      {
        _right = 1;
      }
      break;

    case sf::Keyboard::S:
      if(!_down)
      {
        _down = 1;
      }
      break;

    case sf::Keyboard::Z:
      if(!_up)
      {
        _up = 1;
      }
      break;

    case sf::Keyboard::Up:
      _projUp = 1;
      break;

    case sf::Keyboard::Down:
      _projDown = 1;
      break;

    case sf::Keyboard::Left:
      _projLeft = 1;
      break;

    case sf::Keyboard::Right:
      _projRight = 1;
      break;


    default:
      break;
    }

}

void Player::eventKeyReleased(const sf::Event& event){

  switch (event.key.code)
  {

    case sf::Keyboard::Q:
      if(_left)
      {
        _left = 0;
      }
      break;

    case sf::Keyboard::D:
      if(_right)
      {
        _right = 0;
      }
      break;

    case sf::Keyboard::S:
      if(_down)
      {
        _down = 0;
      }
      break;

    case sf::Keyboard::Z:
      if(_up)
      {
        _up = 0;
      }
      break;

    case sf::Keyboard::Up:
      _projUp = 0;
      break;

    case sf::Keyboard::Down:
      _projDown = 0;
      break;

    case sf::Keyboard::Left:
      _projLeft = 0;
      break;

    case sf::Keyboard::Right:
      _projRight = 0;
      break;

    default:
      break;
    }

}

sf::Texture& Player::getTextureTir()
{
  return _textureTir;
}

void Player::damaged(const int& dmg)
{
  if(_frameInv == 0)
  {
    static Bruitage son;
    son.degats.play();
    _hp -= dmg;
    _frameInv = frame_inv_max;
    //std::cout << "ouch" << std::endl;
  }
}

void Player::drawIhm() const
{
  _ihm.drawMap();
  _ihm.drawLives(_hp, _hpMax);
}


void Player::enterRoomMap(int i, int j)
{
  _ihm.majMap(i, j, 2);
}

void Player::leaveRoomMap(int i, int j)
{
  _ihm.majMap(i, j, 1);
}
