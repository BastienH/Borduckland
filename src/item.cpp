#include "item.hpp"

Item::Item(const std::string& fic, sf::RenderWindow& window, const int& x, const int y, const int& w, const int& h, const int& dmg, const int& atkSpeed, const int& speed, const int& hp)
: Collidable(window, x, y, w, h, 1, 1), _dmg(dmg), _atkSpeed(atkSpeed), _speed(speed), _hp(hp)
{

  _texture.loadFromFile(fic);
  _sprite.setTexture(_texture);
  _sprite.setScale(sf::Vector2f((float)w / _texture.getSize().x, (float)h / _texture.getSize().y));
  _sprite.setPosition(sf::Vector2f(x, y));


}

void Item::pickUp(Player * cible)
{
  if((_dmg < 0 && cible->getDamages() > 1) || (_dmg > 0 && cible->getDamages() < 9))
  {
    cible->setDamages(cible->getDamages() + _dmg);
  }

  if((_atkSpeed < 0 && cible->getCdTirMax() > 10) || (_dmg > 0 && cible->getCdTirMax() < 80))
  {
    cible->setCdTirMax(cible->getCdTirMax() + _atkSpeed);
  }

  if((_speed < 0 && cible->getSpeed() > 1) || (_speed > 0 && cible->getSpeed() < 10))
  {
    cible->setSpeed(cible->getSpeed() + _speed);
  }

  //if(cible->getHpMax() < 24)
  //{
    cible->majHp(_hp);
  //}
}

bool Item::isPlayerNear(Player * cible)
{
  int marge_col = 0;
  int col;

  col =  !(( _x + _w + marge_col < cible->getX())
        || ( _x      - marge_col > cible->getX() + cible->getW())
        || ( _y + _h + marge_col < cible->getY())
        || ( _y      - marge_col > cible->getY() + cible->getH()));

  return col;
}

void Item::init()
{

}

void Item::tick(Room& r)
{
  if(r.isItemPickable())
  {
    _window.draw(_sprite);
    for(std::list<Player *>::iterator it = r.getListPlayer().begin(); it != r.getListPlayer().end(); ++it)
    {
      if(isPlayerNear(*it))
      {
        pickUp(*it);
        r.setItemPickable(false);
        return;
      }
    }
  }

}
