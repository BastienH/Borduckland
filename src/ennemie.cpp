#include "ennemie.hpp"


Mob::Mob(sf::RenderWindow& window, int x, int y, int w, int h)
:Movable(window, x + marge_hitbox/2, y + marge_hitbox/2, w - marge_hitbox, h - marge_hitbox, 6, 6)
{
  //chargement de la texture du monstre
  _texture.loadFromFile("assets/duckmob.png");
  _sprite.setTexture(_texture);
  _sprite.setScale(sf::Vector2f((float)_w / _texture.getSize().x, (float)_h / _texture.getSize().y));
  _sprite.setPosition(sf::Vector2f(_x, _y));
}

void Mob::init()
{

}


void Mob::tick(Room& r)
{
  Player * target = getNearestPlayer(r.getListPlayer());
  std::vector<std::vector<int>> matrice_bool = initColMap(target, r.getColMap());
  std::vector<Tuile> path = PathFinding::aStar(Tuile(_x/taille_grille,_y/taille_grille),Tuile(target->getX() / taille_grille,target->getY()/taille_grille),matrice_bool);

  setMobSpeed(path);
  r.leaveColMat((Collidable*)this);
  doMovement(r.getColMap());
  r.joinColMat((Collidable*)this);

  if(isCollidingWithPlayer(target))
  {
    target->damaged(_damages);
  }

  if(_hp <= 0)
  {
    r.delActor(this);
    r.leaveColMat((Collidable *) this);
  }
}

void Mob::damaged(const int& dmg)
{
  _hp -= dmg;
}


Player * Mob::getNearestPlayer(std::list<Player *>& listePerso)
{
  //On recupere le joueur le plus proche du monstre
  Player * cible = *listePerso.begin();
  int distanceMin = sqrt(pow(_x - cible->getX(),2) + pow(_y - cible->getY(),2));

  for (std::list<Player *>::iterator it = listePerso.begin(); it != listePerso.end(); ++it){
    int distance = sqrt(pow(_x - (*it)->getX(),2) + pow(_y - (*it)->getY(),2));
    if (distance < distanceMin){
      distanceMin = distance;
      cible = *it;
    }
  }

  return cible;
}

std::vector<std::vector<int>> Mob::initColMap(Player * cible, std::array<std::array<Collidable *, mat_j>, mat_i>& matriceCollisions)
{
  std::vector<std::vector<int>> matrice_bool;

  for(int i = 0; i < mat_i; ++i)
  {
    matrice_bool.push_back(std::vector<int>());
    for(int j = 0; j < mat_j; ++j)
    {
      matrice_bool[i].push_back(matriceCollisions[i][j] != nullptr);
    }
  }

  for (int i = cible->getY() / taille_grille; i <= ((cible->getY()+cible->getH()) / taille_grille); i++){
    for (int j = cible->getX() / taille_grille; j <= ((cible->getX()+cible->getW()) / taille_grille); j++){
      if(i >= 0 && i < mat_i && j >= 0 && j < mat_j)
      {
        matrice_bool[i][j] = 0;
      }
    }
  }

  for (int i = this->getY() / taille_grille; i <= ((this->getY()+this->getH()) / taille_grille); i++){
    for (int j = this->getX() / taille_grille; j <= ((this->getX()+this->getW()) / taille_grille); j++){
      if(i >= 0 && i < mat_i && j >= 0 && j < mat_j)
      {
        matrice_bool[i][j] = 0;
      }
    }
  }

  return matrice_bool;
}

void Mob::setMobSpeed(const std::vector<Tuile>& Chemin)
{
  int mh = 0;
  int mv = 0;

  if (Chemin.size()>1)
  {
    if ((_x+_w)/taille_grille > Chemin[1].getX())
    {
      mh = -_speed/2;
    }
    else
    {
      if ((_x-1)/taille_grille  < Chemin[1].getX())
      {
        mh = _speed/2;
      }
    }

    if ((_y+_h)/taille_grille > Chemin[1].getY())
    {
      mv = -_speed/2;
    }
    else
    {
      if ((_y-1)/taille_grille  < Chemin[1].getY())
      {
        mv = _speed/2;
      }
    }
  }

    _vx = mh;
    _vy = mv;
}

bool Mob::isCollidingWithPlayer(Player * cible)
{
  int marge_col = 3;
  int col;

  col =  !(( _x + _w + marge_col < cible->getX())
        || ( _x      - marge_col > cible->getX() + cible->getW())
        || ( _y + _h + marge_col < cible->getY())
        || ( _y      - marge_col > cible->getY() + cible->getH()));

  return col;
}
