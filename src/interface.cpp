#include "interface.hpp"

Interface::Interface(sf::RenderWindow& _window) : window(_window)
{
  textureLeft.loadFromFile("assets/left_heart.png");
  textureRight.loadFromFile("assets/right_heart.png");
  textureLeftCanister.loadFromFile("assets/left_canister.png");
  textureRightCanister.loadFromFile("assets/right_canister.png");

  //init de la matrice/map
  for(int i = 0; i < i_max; ++i)
  {
    map.push_back(std::vector<int>());

    for(int j = 0; j < j_max; ++j)
    {
      map[i].push_back(0);
    }
  }
}


void Interface::drawMap() const
{
  //Dessiner le fond
  sf::RectangleShape fond(sf::Vector2f(1.5 * hInterface, hInterface));
  fond.setPosition(sf::Vector2f(0, hWindow));
  fond.setFillColor(sf::Color(100, 100, 100));
  window.draw(fond);

  //pour toute la carte
  for(int i = 0; i < i_max; ++i)
  {
    for(int j = 0; j < j_max; ++j)
    {
      //si salle découverte
      if(map[i][j])
      {
        //dessiner salle
        sf::RectangleShape rect(sf::Vector2f(1.5 * hInterface/i_max - 2, hInterface/j_max - 2));
        rect.setPosition(sf::Vector2f(j * (1.5 * hInterface/i_max) +1 ,hWindow + i * (hInterface/j_max) + 1));

        //si salle pas courante
        if(map[i][j] == 1)
        {
          rect.setFillColor(sf::Color(100, 250, 50));
        }
        //si salle courante
        else
        {
          rect.setFillColor(sf::Color(255, 0, 0));
        }

        window.draw(rect);
      }
    }
  }
}

void Interface::majMap(int i, int j, int type)
{
  if(i >= 0 && i < i_max && j >= 0 && j < j_max)
  {
    map[i][j] = type;
  }
}

void Interface::drawLives(int hp, int hpmax) const
{
  sf::Sprite coeur;

  for(int i = 0; i < hp; ++i)
  {
    //sf::RectangleShape coeur(sf::Vector2f(20, 20));

    if(i%2 == 0)
    {
      //coeur.setFillColor(sf::Color(255, 0, 0));
      coeur.setTexture(textureLeft);
      coeur.setScale(sf::Vector2f(20.0 / textureLeft.getSize().x, 20.0 / textureLeft.getSize().y));
    }
    else
    {
      coeur.setTexture(textureRight);
      coeur.setScale(sf::Vector2f(20.0 / textureRight.getSize().x, 20.0 / textureRight.getSize().y));
      //coeur.setFillColor(sf::Color(0, 255, 0));
    }

    coeur.setPosition(sf::Vector2f(1.5 * hInterface + 10 + 20*i - (i/12 * 12 * 20), (i/12 * 30) + hWindow + 10));

    window.draw(coeur);

  }

  for(int i = hp; i < hpmax; ++i)
  {
    //sf::RectangleShape coeur(sf::Vector2f(20, 20));

    if(i%2 == 0)
    {
      //coeur.setFillColor(sf::Color(255, 0, 0));
      coeur.setTexture(textureLeftCanister);
      coeur.setScale(sf::Vector2f(20.0 / textureLeft.getSize().x, 20.0 / textureLeft.getSize().y));
    }
    else
    {
      coeur.setTexture(textureRightCanister);
      coeur.setScale(sf::Vector2f(20.0 / textureRight.getSize().x, 20.0 / textureRight.getSize().y));
      //coeur.setFillColor(sf::Color(0, 255, 0));
    }

    coeur.setPosition(sf::Vector2f(1.5 * hInterface + 10 + 20*i - (i/12 * 12 * 20), (i/12 * 30) + hWindow + 10));

    window.draw(coeur);

  }
}
