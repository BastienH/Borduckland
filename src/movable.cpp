#include "movable.hpp"

Movable::Movable(sf::RenderWindow& window, int x, int y, int w, int h, int hpMax, int hp)
: Collidable(window, x, y, w, h, hpMax, hp), _speed(3), _vx(0), _vy(0), _damages(1)
{
}

const int& Movable::getSpeed() const
{
  return _speed;
}

void Movable::setSpeed(const int& speed)
{
  _speed = speed;
}

const int& Movable::getVx() const
{
  return _vx;
}

const int& Movable::getVy() const
{
  return _vy;
}

void Movable::setVx(const int vx)
{
  _vx = vx;
}

void Movable::setVy(const int vy)
{
  _vy = vy;
}

const int& Movable::getDamages() const
{
  return _damages;
}

void Movable::setDamages(const int& damages)
{
  _damages = damages;
}

void Movable::move()
{
  if(_vx || _vy)
  {
    //normalisation
    _vx = _vx * (_speed/sqrt(_vx*_vx + _vy*_vy));
    _vy = _vy * (_speed/sqrt(_vx*_vx + _vy*_vy));

    //déplacement
    _x += _vx;
    _y += _vy;

    _sprite.move(_vx, _vy);
  }
}

void Movable::doMovement(const std::array<std::array<Collidable *, mat_j>, mat_i>& colMap)
{
  int iter = 5;
  Collidable * obj;

  do
  {
    //déplacement
    applySpeed();

    //si pas collision
    if(!(obj = colliding(colMap)))
    {
      //déplacement
      _sprite.move(_vx, _vy);
      return;
    }
    //annuler déplacement
    cancel();

    //diviser la vitesse par 2
    divideSpeed(obj);

    --iter;
  }while(iter);
}


void Movable::applySpeed()
{
  _x += _vx;
  _y += _vy;
}

void Movable::cancel()
{
  _x -= _vx;
  _y -= _vy;
}

Collidable * Movable::colliding(const std::array<std::array<Collidable *, mat_j>, mat_i>& matrice_collisions) const
{
  int iplayer, jplayer, i, j;
  int j1 = _x/taille_grille, j2 = (_x + _w)/taille_grille;
  int i1 = _y/taille_grille, i2 = (_y + _h)/taille_grille;
  int col = 0;

  //pour toutes les cases de la matrices qui sont occupées par le perso
  for(iplayer = i1; iplayer <= i2; ++iplayer)
  {
    for(jplayer = j1; jplayer <= j2; ++jplayer)
    {
      //pour toutes les cases autour
      for(i = -1; i <= 1; ++i)
      {
        for(j = -1; j <= 1; ++j)
        {
          //si on est dans la matrice
          if(iplayer + i >= 0 && iplayer + i < mat_i && jplayer + j >= 0 && jplayer + j < mat_j && matrice_collisions[iplayer + i][jplayer + j] && matrice_collisions[iplayer + i][jplayer + j] != this)
          {
            col =  !(( _x + _w   < matrice_collisions[iplayer+i][jplayer+j]->getX())
                  || ( _x        > matrice_collisions[iplayer+i][jplayer+j]->getX() + matrice_collisions[iplayer+i][jplayer+j]->getW())
                  || ( _y + _h   < matrice_collisions[iplayer+i][jplayer+j]->getY())
                  || ( _y        > matrice_collisions[iplayer+i][jplayer+j]->getY() + matrice_collisions[iplayer+i][jplayer+j]->getH()));

            if(col)
            {
              return matrice_collisions[iplayer+i][jplayer+j];
            }
          }
        }
      }
    }
  }

  return nullptr;
}

void Movable::divideSpeed(Collidable * obj)
{
  //si objet à gauche ou à droite
  if(_x + _w < obj->getX() || _x > obj->getX() + obj->getW())
  {
    _vx /= 2;
  }

  //si obj en haut ou en bas
  if(_y + _h < obj->getY() || _y > obj->getY() + obj->getH())
  {
    _vy /= 2;
  }

}


void Movable::tp(int vx, int vy){

  //si vx ou vy non nul
  if(vx || vy)
  {
    //déplacement
    _x += vx;
    _y += vy;

    _sprite.move(vx, vy);
  }
}
