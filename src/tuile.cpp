#include <sstream>

#include "tuile.hpp"

Tuile::Tuile():_x(0),_y(0),_parentX(0),_parentY(0),_gCost(0),_hCost(0),_fCost(0){}

Tuile::Tuile(int x ,int y):_x(x),_y(y),_parentX(0),_parentY(0),_gCost(0),_hCost(0),_fCost(0){}


float Tuile::getFCost() const{
    return _fCost;
}

int Tuile::getX() const{
    return _x;
}

int Tuile::getY() const{
    return _y;
}

void Tuile::setX(int x){
    _x = x;
}

void Tuile::setY(int y){
    _y = y;
}

bool operator<(Tuile const &a, Tuile const &b){
    return a.getFCost() < b.getFCost();
}