#include "lvl_generator.hpp"

#include <iostream>
#include <cstdlib>
#include <iterator>
#include <ctime>

#include "player.hpp"

LevelGeneration::LevelGeneration(const int& nbRoom, sf::RenderWindow& window): rooms(), takenPositions(), _nbRoom( (nbRoom <= i_max * j_max ? nbRoom : i_max * j_max)), _window(window)
{

  //init de la matrice de salles
  for(int i = 0; i < i_max; ++i)
  {
    for(int j = 0; j < j_max; ++j)
    {
      rooms[i][j] = nullptr;
    }
  }

}

LevelGeneration::~LevelGeneration()
{
  for(int i = 0; i < i_max; ++i)
  {
    for(int j = 0; j < j_max; ++j)
    {
      delete rooms[i][j];
    }
  }
}

void LevelGeneration::start(int seed)
{
  srand(seed);
  //génération des salles
  createRooms();
  //génération des portes
  setDoors();

  /*for(int i = 0; i < i_max; ++i)
  {
    for(int j = 0; j < j_max; ++j)
    {
      std::cout << (rooms[i][j] != nullptr) << " ";
    }
    std::cout << std::endl;
  }*/


}

void LevelGeneration::createRooms()
{

  int iter = 0;

  rooms[origine_x][origine_y] = new Room(_window, true);

  takenPositions.push_back({origine_x, origine_y});
  std::vector<int> checkingPos = {0, 0};

  for(int i = 0; i < _nbRoom; ++i)
  {

    checkingPos = newPosition();

    if(numberOfNeighbors(checkingPos) > 1)
    {
      iter = 0;

      do
      {
        checkingPos = newSelectivePosition();
        ++iter;

      }while(numberOfNeighbors(checkingPos) > 1 && iter < 100);
    }

    rooms[checkingPos[0]][checkingPos[1]] = new Room(_window);
    takenPositions.push_back(checkingPos);
  }

}

std::vector<int> LevelGeneration::newPosition()
{
  int x = origine_x, y = origine_y;
  std::vector<int> checkingPos = {0, 0};
  int index;
  bool upDown, positive;
  std::vector<std::vector<int>>::iterator it;


  do
  {
    index = rand() % takenPositions.size();
    x = takenPositions[index][0];
    y = takenPositions[index][1];

    upDown = rand()%100 < 50;
    positive = rand()%100 < 50;

    if(upDown)//recherche horizontale
    {
      if(positive)
      {
        y += 1;
      }
      else
      {
        y -= 1;
      }
    }
    else //recherche vecticale
    {
      if(positive)
      {
        x += 1;
      }
      else
      {
        x -= 1;
      }
    }

    checkingPos[0] = x;
    checkingPos[1] = y;

    it = std::find(takenPositions.begin(), takenPositions.end(),checkingPos);

  }while(it != takenPositions.end() && x > 0 && x <= i_max && y > 0 && y <= j_max);



  return checkingPos;
}

std::vector<int> LevelGeneration::newSelectivePosition()
{

  int x = origine_x, y = origine_y;
  std::vector<int> checkingPos = {0, 0};
  int index = 0, inc = 0;
  bool upDown, positive;
  std::vector<std::vector<int>>::iterator it;

  do
  {
    inc = 0;

    do
    {
      index = rand() % takenPositions.size();
      ++inc;

    }while(numberOfNeighbors(takenPositions[index]) > 1 && inc < 100);

    x = takenPositions[index][0];
    y = takenPositions[index][1];

    upDown = rand()%100 < 50;
    positive = rand()%100 < 50;

    if(upDown)//recherche horizontale
    {
      if(positive)
      {
        y += 1;
      }
      else
      {
        y -= 1;
      }
    }
    else //recherche vecticale
    {
      if(positive)
      {
        x += 1;
      }
      else
      {
        x -= 1;
      }
    }

    checkingPos[0] = x;
    checkingPos[1] = y;

    it = std::find(takenPositions.begin(), takenPositions.end(),checkingPos);

  }while(it != takenPositions.end() || x < 0 || x >= i_max || y < 0 || y >= j_max);

  return checkingPos;
}

int LevelGeneration::numberOfNeighbors(const std::vector<int>& checkingPos)
{
  int retour = 0;

  std::vector<int> neighbors = {checkingPos[0], checkingPos[1]};

  std::vector<std::vector<int>>::iterator it;

  //haut
  neighbors[0] -= 1;
  it = std::find(takenPositions.begin(), takenPositions.end(),neighbors);
  if(it != takenPositions.end())
  {
    ++retour;
  }

  //bas
  neighbors[0] += 1;
  it = std::find(takenPositions.begin(), takenPositions.end(),neighbors);
  if(it != takenPositions.end())
  {
    ++retour;
  }

  //gauche
  neighbors[1] -= 1;
  it = std::find(takenPositions.begin(), takenPositions.end(),neighbors);
  if(it != takenPositions.end())
  {
    ++retour;
  }

  //droite
  neighbors[1] += 1;
  it = std::find(takenPositions.begin(), takenPositions.end(),neighbors);
  if(it != takenPositions.end())
  {
    ++retour;
  }

  return retour;

}


void LevelGeneration::setDoors()
{
  int l_mur = 30;

  for(int x = 0; x < i_max; ++x)
  {
    for(int y = 0; y < j_max; ++y)
    {
      if(rooms[x][y])
      {

        //porte est
        if(y + 1 < j_max && rooms[x][y + 1])
        {
          rooms[x][y]->addObstacle("assets/Vwall.png", wWindow - l_mur, 0, l_mur, 1.0/3*hWindow, false);
          rooms[x][y]->addDoor("assets/doorV.png", wWindow - l_mur, 1.0/3*hWindow, l_mur, 1.0/3*hWindow);
          rooms[x][y]->addObstacle("assets/Vwall.png", wWindow - l_mur, 2.0/3*hWindow, l_mur, 1.0/3*hWindow, false);
        }
        else
        {
          rooms[x][y]->addObstacle("assets/Vwall.png", wWindow - l_mur, 0, l_mur, hWindow, false);
        }

        //porte ouest
        if(y - 1 >= 0 && rooms[x][y - 1])
        {
          rooms[x][y]->addObstacle("assets/Vwall.png", 0, 0, l_mur, 1.0/3*hWindow, false);
          rooms[x][y]->addDoor("assets/doorV.png", 0, 1.0/3*hWindow, l_mur, 1.0/3*hWindow);
          rooms[x][y]->addObstacle("assets/Vwall.png", 0, 2.0/3*hWindow, l_mur, 1.0/3*hWindow, false);
        }
        else
        {
          rooms[x][y]->addObstacle("assets/Vwall.png", 0, 0, l_mur, hWindow, false);
        }

        //porte sud
        if(x + 1 < i_max && rooms[x + 1][y])
        {
          rooms[x][y]->addObstacle("assets/Hwall.png", 0, hWindow - l_mur, 1.0/3*wWindow, l_mur, false);
          rooms[x][y]->addDoor("assets/doorH.png", 1.0/3*wWindow, hWindow - l_mur, 1.0/3*wWindow, l_mur);
          rooms[x][y]->addObstacle("assets/Hwall.png", 2.0/3*wWindow, hWindow - l_mur, 1.0/3*wWindow, l_mur, false);
        }
        else
        {
          rooms[x][y]->addObstacle("assets/Hwall.png", 0, hWindow - l_mur, wWindow, l_mur, false);
        }

        //porte nord
        if(x - 1 >= 0 && rooms[x - 1][y])
        {
          rooms[x][y]->addObstacle("assets/Hwall.png", 0, 0, 1.0/3*wWindow, l_mur, false);
          rooms[x][y]->addDoor("assets/doorH.png", 1.0/3*wWindow, 0, 1.0/3*wWindow, l_mur);
          rooms[x][y]->addObstacle("assets/Hwall.png", 2.0/3*wWindow, 0, 1.0/3*wWindow, l_mur, false);
        }
        else
        {
          rooms[x][y]->addObstacle("assets/Hwall.png", 0, 0, wWindow, l_mur, false);
        }

        rooms[x][y]->addObstacle("assets/cornerNO.png", 0, 0, l_mur, l_mur, false);
        rooms[x][y]->addObstacle("assets/cornerNE.png", wWindow - l_mur, 0, l_mur, l_mur, false);
        rooms[x][y]->addObstacle("assets/cornerSO.png", 0, hWindow - l_mur, l_mur, l_mur, false);
        rooms[x][y]->addObstacle("assets/cornerSE.png", wWindow - l_mur, hWindow - l_mur, l_mur, l_mur, false);

        //rooms[x][y]->affCol();
      }

    }
  }
}

Room * & LevelGeneration::getRoom(int i, int j){
  return rooms[i][j];
}


void LevelGeneration::draw(int i, int j)
{
  //std::cout << rooms[i][j];

  //pour tous les obstacles sur la salle active
  if(rooms[i][j])
  {
    rooms[i][j]->draw();
  }
}


void LevelGeneration::moveView(std::vector<RoomLoad>::iterator& loadedRoom){

  //int mvt_x = 0, mvt_y = 0;
  std::list<Player *>::iterator begin = rooms[loadedRoom->_i][loadedRoom->_j]->getListPlayer().begin();
  std::list<Player *>::iterator end = rooms[loadedRoom->_i][loadedRoom->_j]->getListPlayer().end();
  std::list<Player *>::iterator courPlayer = begin;

  int i = loadedRoom->_i, j = loadedRoom->_j;

  while(courPlayer != end)
  {
    if((*courPlayer)->getX() < 5)
    {
      rooms[loadedRoom->_i][loadedRoom->_j]->leaveColMat(*courPlayer);
      loadedRoom->_j -= 1;
      rooms[loadedRoom->_i][loadedRoom->_j]->newPlayer(*courPlayer);
      (*courPlayer)->tp(wWindow - 50 - (*courPlayer)->getW(), 0);
      (*courPlayer)->viewPlayer1.reset(sf::FloatRect(0, 0, wWindow, hWindow));

      (*courPlayer)->enterRoomMap(loadedRoom->_i, loadedRoom->_j);
      (*courPlayer)->leaveRoomMap(i, j);

      courPlayer = rooms[i][j]->delPlayerIt(courPlayer);
    }
    else if(((*courPlayer)->getX() + (*courPlayer)->getW()) > wWindow - 5)
    {
      rooms[loadedRoom->_i][loadedRoom->_j]->leaveColMat(*courPlayer);
      loadedRoom->_j += 1;
      rooms[loadedRoom->_i][loadedRoom->_j]->newPlayer(*courPlayer);
      (*courPlayer)->tp(- wWindow + 50 + (*courPlayer)->getW(), 0);
      (*courPlayer)->viewPlayer1.reset(sf::FloatRect(0, 0, wWindow, hWindow));

      (*courPlayer)->enterRoomMap(loadedRoom->_i, loadedRoom->_j);
      (*courPlayer)->leaveRoomMap(i, j);

      courPlayer = rooms[i][j]->delPlayerIt(courPlayer);
    }
    else if((*courPlayer)->getY() < 5)
    {
      rooms[loadedRoom->_i][loadedRoom->_j]->leaveColMat(*courPlayer);
      loadedRoom->_i -= 1;
      rooms[loadedRoom->_i][loadedRoom->_j]->newPlayer(*courPlayer);
      (*courPlayer)->tp(0, hWindow - 50 - (*courPlayer)->getH());
      (*courPlayer)->viewPlayer1.reset(sf::FloatRect(0, 0, wWindow, hWindow));

      (*courPlayer)->enterRoomMap(loadedRoom->_i, loadedRoom->_j);
      (*courPlayer)->leaveRoomMap(i, j);

      courPlayer = rooms[i][j]->delPlayerIt(courPlayer);
    }
    else if(((*courPlayer)->getY() + (*courPlayer)->getH()) > hWindow - 5)
    {
      rooms[loadedRoom->_i][loadedRoom->_j]->leaveColMat(*courPlayer);
      loadedRoom->_i += 1;
      rooms[loadedRoom->_i][loadedRoom->_j]->newPlayer(*courPlayer);
      (*courPlayer)->tp(0, -hWindow + 50 + (*courPlayer)->getH());
      (*courPlayer)->viewPlayer1.reset(sf::FloatRect(0, 0, wWindow, hWindow));

      (*courPlayer)->enterRoomMap(loadedRoom->_i, loadedRoom->_j);
      (*courPlayer)->leaveRoomMap(i, j);

      courPlayer = rooms[i][j]->delPlayerIt(courPlayer);
    }
    else
    {
      ++courPlayer;
    }

  }
}


void LevelGeneration::spawnPlayer(int i, int j)
{
  if(rooms[i][j])
  {
    Player * p = new Player(_window, wWindow/2, hWindow/2, 50, 75);
    p->enterRoomMap(i, j);
    rooms[i][j]->newPlayer(p);
    _loaded.push_back(RoomLoad(i, j));
  }
}

void LevelGeneration::tick()
{
  for(std::vector<RoomLoad>::iterator loadedRoom = _loaded.begin(); loadedRoom != _loaded.end(); ++loadedRoom)
  {
    rooms[loadedRoom->_i][loadedRoom->_j]->tick();
    moveView(loadedRoom);
  }
}
