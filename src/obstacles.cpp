#include "obstacles.hpp"

//Obstacles

Obstacles::Obstacles(const std::string& fic, sf::RenderWindow& window, const int& x, const int& y, const int& w, const int& h, const bool& destroyable)
: Collidable(window, x, y, w, h, 3, 3), _canBeDamaged(destroyable)
{

    _texture.loadFromFile(fic);
    _sprite.setTexture(_texture);

    _sprite.setPosition(sf::Vector2f(x, y));
    _sprite.setScale(sf::Vector2f((float)w / _texture.getSize().x, (float)h / _texture.getSize().y));

}

void Obstacles::damaged(const int& dmg)
{
  (void) dmg;
  if(_canBeDamaged)
  {
    _hp -= 1;
  }

}
