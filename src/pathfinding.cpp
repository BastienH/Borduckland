#include "pathfinding.hpp"

bool PathFinding::isValid(int x, int y, std::vector< std::vector< int >> matriceCollisions) { //If our Tuile is an obstacle it is not valid

	if (x < 0 || y < 0 || x >= (mat_j) || y >= (mat_i)  || (bool)matriceCollisions[y][x]) {
		return false;
	}
	return true;

}

bool PathFinding::isDestination(int x, int y, Tuile dest) {

	if (x == dest._x && y == dest._y) {
		return true;
	}
	return false;
}

double PathFinding::calculateH(int x, int y, Tuile dest) {
	double H = (sqrt((x - dest._x)*(x - dest._x)
		+ (y - dest._y)*(y - dest._y)));
	return H;
}

std::vector<Tuile> PathFinding::makePath(std::array<std::array<Tuile, mat_i>, mat_j > map, Tuile dest) {
	std::vector<Tuile> usablePath;
	try {
		//std::cout << "Found a path" << std::endl;
		int x = dest._x;
		int y = dest._y;
		std::stack<Tuile> path;

		while (!(map[x][y]._parentX == x && map[x][y]._parentY == y)
			&& map[x][y]._x != -1 && map[x][y]._y != -1)
		{
			path.push(map[x][y]);
			int tempX = map[x][y]._parentX;
			int tempY = map[x][y]._parentY;
			x = tempX;
			y = tempY;

		}
		path.push(map[x][y]);

		while (!path.empty()) {
			Tuile top = path.top();
			path.pop();
			usablePath.emplace_back(top);
		}
	}
	catch(const std::exception& e){
		std::cout << e.what() << std::endl;
	}
	return usablePath;
}

std::vector<Tuile> PathFinding::aStar(Tuile player, Tuile dest,std::vector< std::vector< int >> matriceCollisions) {


	std::vector<Tuile> empty;

	if (isValid(dest._x, dest._y, matriceCollisions) == false) {
		//std::cout << "Destination is an obstacle" << std::endl;
		return empty;
		//Destination is invalid
	}

	if (isDestination(player._x, player._y, dest)) {
		//std::cout << "You are the destination" << std::endl;
		return empty;
		//You clicked on yourself
	}

	bool closedList[(mat_j)][(mat_i)];

	//Initialize whole map
	std::array<std::array < Tuile, mat_i>, mat_j> allMap;
	for (int x = 0; x < (mat_j ); x++) {
		for (int y = 0; y < (mat_i); y++) {
			allMap[x][y]._fCost = FLT_MAX;
			allMap[x][y]._gCost = FLT_MAX;
			allMap[x][y]._hCost = FLT_MAX;
			allMap[x][y]._parentX = -1;
			allMap[x][y]._parentY = -1;
			allMap[x][y]._x = x;
			allMap[x][y]._y = y;

			closedList[x][y] = false;
		}
	}

	//Initialize our starting list
	int x = player._x;
	int y = player._y;
	allMap[x][y]._fCost = 0.0;
	allMap[x][y]._gCost = 0.0;
	allMap[x][y]._hCost = 0.0;
	allMap[x][y]._parentX = x;
	allMap[x][y]._parentY = y;

	std::vector<Tuile> openList;
	openList.emplace_back(allMap[x][y]);
	bool destinationFound = false;

	while (!openList.empty()&&openList.size()<(mat_j )*(mat_i)) {
		Tuile tuile;
		do {
			float temp = FLT_MAX;
			std::vector<Tuile>::iterator itTuile;
			for (std::vector<Tuile>::iterator it = openList.begin();
				it != openList.end(); it = next(it)) {
				Tuile n = *it;
				if (n._fCost < temp) {
					temp = n._fCost;
					itTuile = it;
				}
			}
			tuile = *itTuile;
			openList.erase(itTuile);
		} while (isValid(tuile._x, tuile._y, matriceCollisions) == false);

		x = tuile._x;
		y = tuile._y;
		closedList[x][y] = true;

		//For each neighbour starting from North-West to South-East
		for (int newX = -1; newX <= 1; newX++) {
			for (int newY = -1; newY <= 1; newY++) {
				if(newX != newY)
				{
					double gNew, hNew, fNew;
					if (isValid(x + newX, y + newY, matriceCollisions)) {
						if (isDestination(x + newX, y + newY, dest))
						{
							//Destination found - make path
							allMap[x + newX][y + newY]._parentX = x;
							allMap[x + newX][y + newY]._parentY = y;
							destinationFound = true;
							return makePath(allMap, dest);
						}
						else if (closedList[x + newX][y + newY] == false)
						{
							gNew = tuile._gCost + 1.0;
							hNew = calculateH(x + newX, y + newY, dest);
							fNew = gNew + hNew;
							// Check if this path is better than the one already present
							if (allMap[x + newX][y + newY]._fCost == FLT_MAX ||
								allMap[x + newX][y + newY]._fCost > fNew)
							{
									// Update the details of this neighbour tuile
									allMap[x + newX][y + newY]._fCost = fNew;
									allMap[x + newX][y + newY]._gCost = gNew;
									allMap[x + newX][y + newY]._hCost = hNew;
									allMap[x + newX][y + newY]._parentX = x;
									allMap[x + newX][y + newY]._parentY = y;
									openList.emplace_back(allMap[x + newX][y + newY]);
							}
						}
					}
				}
			}
		}
	}
	if (destinationFound == false) {
		//std::cout << "Destination not found" << std::endl;
	}
	return empty;
}
