SRC=$(wildcard src/*.cpp)
EXE=main

CXXFLAGS+=-Wall -Wextra -g -std=c++11
LDFLAGS=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfml-network -ljsoncpp

OBJ=$(addprefix build/,$(SRC:.cpp=.o))
DEP=$(addprefix build/,$(SRC:.cpp=.d))

all: $(OBJ)
	$(CXX) -o $(EXE) $^ $(LDFLAGS)

build/%.o: %.cpp
	@mkdir -p build
	@mkdir -p build/src
	$(CXX) $(CXXFLAGS) -o $@ -c $< -I"./include"

clean:
	rm -rf build core *.gch $(EXE)

-include $(DEP)
